import requests
import time
import sys

def build():
    print("building started ...")
    time.sleep(2)
    print('Calling url')
    resp = requests.get('https://jsonplaceholder.typicode.com/todos')
    print('url status: ', resp.status_code)
    time.sleep(2)
    print('build finished...')


def test():
    print('test 1 passed...')
    time.sleep(2)
    print('test 2 passed ')


def deploy():
    print('deploying the project')
    time.sleep(2)
    print('deployment successful')


if __name__ == "__main__":
    arguments = sys.argv
    if len(arguments) > 1:
        cmd = arguments[1]
        if cmd == 'build':
            build()
        elif cmd == 'test':
            test()
        elif cmd == 'deploy':
            deploy()
        else:
            test()
    else:
        test()
    # simulate_build()
